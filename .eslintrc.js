module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'airbnb-base',
    ],
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    plugins: [
        '@typescript-eslint',
    ],
    rules: {
        indent: ['warn', 4],
        'import/extensions': [0],
        'import/no-unresolved': [0],
        'class-methods-use-this': [0],
        'import/prefer-default-export': [0],
        'no-param-reassign': [0],
        'max-len': ['error', { code: 180 }],
        'no-tabs': [0],
        'no-unused-vars': [0],
        '@typescript-eslint/no-unused-vars': ['error'],
        'no-shadow': 'off',
        '@typescript-eslint/no-shadow': ['error'],
    },
};
