import errors from '../constants/errors';

export default class NotFoundError extends Error {
    constructor(message: string, statusCode: number = 404) {
        super(message);
        this.name = errors.NOT_FOUND_ERROR;
        this.statusCode = statusCode;
    }
}
