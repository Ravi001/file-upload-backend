import errors from '../constants/errors';

export default class ValidationError extends Error {
    errors: Array<string>;

    constructor(message: string, errorList: Array<string> = [], statusCode: number = 400) {
        super(message);
        this.name = errors.VALIDATION_ERROR;
        this.statusCode = statusCode;
        this.errors = errorList;
    }
}
