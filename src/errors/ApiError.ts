import errors from '../constants/errors';

export default class ApiError extends Error {
    constructor(message: string, statusCode: number = 400) {
        super(message);
        this.name = errors.API_ERROR;
        this.statusCode = statusCode;
    }
}
