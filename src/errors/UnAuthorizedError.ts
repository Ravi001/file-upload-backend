import errors from '../constants/errors';

export default class UnAuthorizedError extends Error {
    constructor(message: string = 'Not Authorized to Access', statusCode: number = 401) {
        super(message);
        this.name = errors.UNAUTHORIZED_ERROR;
        this.statusCode = statusCode;
    }
}
