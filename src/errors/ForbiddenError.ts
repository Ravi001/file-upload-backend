import errors from '../constants/errors';

export default class ForbiddenError extends Error {
    constructor(message: string, statusCode: number = 403) {
        super(message);
        this.name = errors.FORBIDDEN_ERROR;
        this.statusCode = statusCode;
    }
}
