import errors from '../constants/errors';

export default class BadRequestError extends Error {
    constructor(message: string, statusCode: number = 400) {
        super(message);
        this.name = errors.BAD_REQUEST_ERROR;
        this.statusCode = statusCode;
    }
}
