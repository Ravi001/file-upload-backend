import {
    GetObjectCommandInput,
    GetObjectCommand,
    GetObjectCommandOutput,
    PutObjectCommandInput,
    PutObjectCommand,
    PutObjectCommandOutput,
    CreateMultipartUploadCommand,
    CreateMultipartUploadCommandOutput,
    CreateMultipartUploadCommandInput,
    UploadPartCommandInput,
    UploadPartCommand,
    UploadPartCommandOutput,
    CompleteMultipartUploadCommand,
    CompleteMultipartUploadCommandInput,
    CompleteMultipartUploadCommandOutput,
    CompletedMultipartUpload,
    AbortMultipartUploadCommandInput,
    AbortMultipartUploadCommand,
    AbortMultipartUploadCommandOutput,
} from '@aws-sdk/client-s3';
import { Readable } from 'stream';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import client from './s3';

/**
 * This Function Reads File From S3
 * @param bucket
 * @param key
 * @returns Promise<GetObjectCommandOutput>
 */
export const getObject = async (Bucket: string, Key: string): Promise<GetObjectCommandOutput> => {
    const params: GetObjectCommandInput = {
        Bucket,
        Key,
    };
    return client.send(new GetObjectCommand(params));
};

/**
 * This Function initiates Multipart Upload (Returns UploadId which can be used to continue multipart upload)
 * @param bucket
 * @param key
 * @returns
 */
export const initiateMultipartUpload = async (Bucket: string, Key: string): Promise<CreateMultipartUploadCommandOutput> => {
    const params: CreateMultipartUploadCommandInput = {
        Bucket,
        Key,
    };
    return client.send(new CreateMultipartUploadCommand(params));
};

/**
 * This Function continues Multipart Upload Requires UploadId and PartNumber
 * @param Bucket
 * @param Key
 * @param Body
 * @param UploadId
 * @param PartNumber
 * @returns
 */
export const multipartUpload = async (Bucket: string, Key: string, Body: Buffer | Readable, UploadId: string, PartNumber: number): Promise<UploadPartCommandOutput> => {
    const params: UploadPartCommandInput = {
        Bucket,
        Key,
        Body,
        PartNumber,
        UploadId,
    };
    return client.send(new UploadPartCommand(params));
};

/**
 * This Function Completes Multipart Upload
 * @param Bucket
 * @param Key
 * @param UploadId
 * @param MultipartUpload
 * @returns
 */
export const completeMultipartUpload = async (
    Bucket: string,
    Key: string,
    UploadId: string,
    MultipartUpload: CompletedMultipartUpload,
): Promise<CompleteMultipartUploadCommandOutput> => {
    const params : CompleteMultipartUploadCommandInput = {
        Bucket,
        Key,
        UploadId,
        MultipartUpload,
    };
    return client.send(new CompleteMultipartUploadCommand(params));
};

/**
 * This Function Aborts Multipart Upload
 * @param Bucket
 * @param Key
 * @param UploadId
 * @returns
 */
export const abortMultipartUpload = async (Bucket: string, Key: string, UploadId: string): Promise<AbortMultipartUploadCommandOutput> => {
    const params : AbortMultipartUploadCommandInput = {
        Bucket,
        Key,
        UploadId,
    };
    return client.send(new AbortMultipartUploadCommand(params));
};

/**
 * This Functions Uploads Object to s3
 * @param Bucket Bucket name
 * @param Key File name
 * @param Body Actual File
 * @param publicObject Whether Object should public(read access) or private
 * @returns Promise<PutObjectCommandOutput>
 */
export const putObject = async (Bucket: string, Key: string, Body: Buffer | Readable, publicObject: boolean = false): Promise<PutObjectCommandOutput> => {
    const params: PutObjectCommandInput = {
        Bucket: Bucket || process.env.AWS_BUCKET,
        Key,
        Body,
        ACL: publicObject ? 'public-read' : 'private',
    };
    return client.send(new PutObjectCommand(params));
};

/**
 * This Function Generates S3 Url Based on Bucket and Key Passed
 * @param bucket Buckent Name
 * @param key File Name
 * @returns URL
 */
export const getS3Url = (bucket: string, key: string) => {
    const region = process.env.AWS_REGION;
    const regionString = region.includes('us-east-1') ? '' : `-${region}`;
    return `https://${bucket}.s3${regionString}.amazonaws.com/${key}`;
};

/**
 * Get S3 presigned url
 * @param Bucket string
 * @param Key string
 * @param expiresIn number
 * @returns string
 */
export const getPresignedS3Url = async (Key: string, Bucket: string = process.env.AWS_BUCKET as string, expiresIn: number = 300) => {
    const command = new GetObjectCommand({ Bucket, Key });
    return getSignedUrl(client, command, { expiresIn });
};
