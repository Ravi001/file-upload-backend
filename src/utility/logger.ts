import fs from 'fs';
import moment from 'moment';
import winston from 'winston';
import 'winston-daily-rotate-file';

const { combine, timestamp, printf } = winston.format;

const format = printf(({ level, message }) => {
    if (level.toUpperCase() !== 'ERROR') {
        return `{ "Timestamp" : "${moment().format('lll')}" , "Level" : "${level.toUpperCase()}" , Message : "${message}" }`;
    }
    return `{ "Timestamp" : "${moment().format('lll')}" , "Level" : "${level.toUpperCase()}" , Message : "${message}" }`;
});

const datePattern = 'DD-MM-YYYY';
const dir = 'logs';

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

const fileTransport = new winston.transports.DailyRotateFile({
    filename: 'logs/log-%DATE%.log',
    datePattern,
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '30d',
});

class Logger {
    private logger;

    constructor() {
        this.logger = winston.createLogger({
            format: combine(timestamp(), format),

            defaultMeta: { service: 'admin-backend-service' },
            transports: [new winston.transports.Console({}), fileTransport],
            exitOnError: false,
        });
    }

    error(message: Object) {
        const parsedMessage = this.parseMessage(message);
        this.logger.error(parsedMessage);
    }

    info(message: Object) {
        const parsedMessage = this.parseMessage(message);
        this.logger.info(parsedMessage);
    }

    private parseMessage(message: any) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        }
        return message;
    }
}

export default new Logger();
