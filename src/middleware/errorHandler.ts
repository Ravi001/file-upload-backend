import {
    NextFunction, Request, Response,
} from 'express';
import ApiError from '../errors/ApiError';
import BadRequestError from '../errors/BadRequestError';
import ForbiddenError from '../errors/ForbiddenError';
import NotFoundError from '../errors/NotFoundError';
import UnAuthorizedError from '../errors/UnAuthorizedError';
import ValidationError from '../errors/ValidationError';

const errorHandler = (
    err: ApiError | BadRequestError | ForbiddenError | NotFoundError | UnAuthorizedError | ValidationError | Error,
    req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    next: NextFunction,
) => {
    if (err && err instanceof ValidationError && err.errors.length > 0) {
        return res.status(err.statusCode || 400).send({
            success: false,
            message: err.message,
            errors: err.errors,
        });
    }

    return res.status(err.statusCode || 400).send({
        success: false,
        message: err.message,
    });
};

export default errorHandler;
