import { Request, Response, NextFunction } from 'express';
import logger from '../utility/logger';

const requestLogger = (req: Request, res: Response, next: NextFunction) => {
    const oldWrite = res.write;
    const oldEnd = res.end;

    const chunks: any[] = [];

    res.write = (...restArgs: any): any => {
        chunks.push(Buffer.from(restArgs[0]));
        oldWrite.apply(res, restArgs);
    };

    res.end = (...restArgs: any) : any => {
        if (restArgs[0]) {
            chunks.push(Buffer.from(restArgs[0]));
        }
        const body = Buffer.concat(chunks).toString('utf8');
        const contentType = res.get('Content-Type');
        let responseBody = body;
        if (contentType && contentType.includes('application/json')) {
            responseBody = JSON.parse(body);
        }
        if (contentType && (contentType.includes('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') || contentType.includes('application/zip'))) {
            responseBody = '';
        }
        const logObject = {
            Path: req.baseUrl + req.path,
            Url: req.baseUrl + req.url,
            Query: req.query,
            RequestMethod: req.method,
            RequestBody: req.body,
            RequestHeaders: req.headers,
            RequestParams: req.params,
            RequestBaseURL: req.hostname,
            ResponseStatusCode: res.statusCode,
            ResponseStatusMessage: res.statusMessage,
            ResponseHeaders: res.getHeaders(),
            ResponseContentType: contentType,
            ResponseBody: responseBody,
        };

        if (res.statusCode < 400) {
            logger.info(JSON.stringify(logObject));
        } else {
            logger.error(JSON.stringify(logObject));
        }
        oldEnd.apply(res, restArgs);
    };

    next();
};

export default requestLogger;
