export default {
    API_ERROR: 'Api Error',
    FORBIDDEN_ERROR: 'Forbidden Error',
    BAD_REQUEST_ERROR: 'BadRequest Error',
    NOT_FOUND_ERROR: 'NotFound Error',
    UNAUTHORIZED_ERROR: 'UnAuthorized Error',
    VALIDATION_ERROR: 'Vaidation Error',
    INTERNAL_SERVER_ERROR: 'Internal Server Error',
};
