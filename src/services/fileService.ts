import { Request } from 'express';
import BadRequestError from '../errors/BadRequestError';
import {
    abortMultipartUpload, completeMultipartUpload, initiateMultipartUpload, multipartUpload,
} from '../utility/s3Operations';

export default class FileService {
    async initiateFileUpload(req: Request) {
        const { name } = req.body;
        const { UploadId } = await initiateMultipartUpload(process.env.AWS_BUCKET, name);
        return { success: true, data: UploadId };
    }

    async continueFileUpload(req: Request) {
        const { uploadId, partNumber, name } = req.body;
        const { file } = req;
        if (file && file.buffer) {
            const { ETag } = await multipartUpload(process.env.AWS_BUCKET, name, file?.buffer, uploadId, partNumber);
            return { success: true, data: { eTag: ETag, partNumber } };
        }
        throw new BadRequestError('File Not Present');
    }

    async completeFileUpload(req: Request) {
        const { uploadId, parts, name } = req.body;
        await completeMultipartUpload(process.env.AWS_BUCKET, name, uploadId, { Parts: parts });
        return { success: true, data: 'File Uploaded Successfully' };
    }

    async abortFileUpload(req: Request) {
        const { uploadId, name } = req.body;
        await abortMultipartUpload(process.env.AWS_BUCKET, name, uploadId);
        return { success: true, data: 'File Upload Aborted' };
    }
}
