/* eslint-disable no-unused-vars */
declare global {
	namespace NodeJS {
		interface ProcessEnv {
			AWS_BUCKET: string;
			AWS_ACCESS_KEY: string;
			AWS_SECRET_KEY: string;
			AWS_REGION: string;
		}
	}
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export { };
