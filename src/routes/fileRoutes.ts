import { Router } from 'express';
import multer from 'multer';
import FileController from '../controllers/fileController';

const upload = multer({});

const fileRoutes = Router();
const fileController = new FileController();

fileRoutes.post('/start', fileController.initiateFileUpload);
fileRoutes.post('/upload', upload.single('file'), fileController.continueFileUpload);
fileRoutes.post('/finish', fileController.completeFileUpload);
fileRoutes.post('/abort', fileController.abortFileUpload);

export default fileRoutes;
