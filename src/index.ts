/* eslint-disable import/first */
import dotenv from 'dotenv';
import path from 'path';

dotenv.config({ path: path.resolve(__dirname, '../config/.env') });

import express, { NextFunction, Request, Response } from 'express';
import cors from 'cors';

import requestLogger from './middleware/requestLogger';
import errorHandler from './middleware/errorHandler';
import NotFoundError from './errors/NotFoundError';
import logger from './utility/logger';
import fileRoutes from './routes/fileRoutes';

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(requestLogger);
app.get('/health', (_: express.Request, res: express.Response) => res.status(200).send({ success: true, message: 'Service is Running' }));
app.use('/v1/files', fileRoutes);
app.use('**', (_req: Request, _res: Response, next: NextFunction) => {
    next(new NotFoundError('Route Not Found'));
});
app.use(errorHandler);

const PORT = process.env.PORT || 9999;

app.listen(PORT, () => logger.info(`Server is up and Listening on PORT ${PORT}`));
