import { NextFunction, Request, Response } from 'express';
import FileService from '../services/fileService';

export default class FileController {
    private fileService: FileService;

    constructor() {
        this.fileService = new FileService();
    }

    initiateFileUpload = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.fileService.initiateFileUpload(req);
            res.status(200).send(response);
        } catch (error: unknown) {
            next(error);
        }
    };

    continueFileUpload = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.fileService.continueFileUpload(req);
            res.status(200).send(response);
        } catch (error: unknown) {
            next(error);
        }
    };

    completeFileUpload = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.fileService.completeFileUpload(req);
            res.status(200).send(response);
        } catch (error: unknown) {
            next(error);
        }
    };

    abortFileUpload = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.fileService.abortFileUpload(req);
            res.status(200).send(response);
        } catch (error: unknown) {
            next(error);
        }
    };
}
